#include <stdio.h>
#include <CoreFoundation/CoreFoundation.h>

void debugCFRelease(CFTypeRef obj)
{
    CFRelease(obj);
    printf("Releasing: %tx\n", (ptrdiff_t)obj);
}
