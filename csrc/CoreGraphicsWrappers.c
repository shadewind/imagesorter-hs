#include <stdio.h>
#include <CoreGraphics/CoreGraphics.h>

void CGContextFillRectW(CGContextRef c,
                       const CGRect *rect)
{ CGContextFillRect(c, *rect); }

void CGContextFillEllipseInRectW(CGContextRef context,
                                 const CGRect *rect)
{ CGContextFillEllipseInRect(context, *rect); }

void CGContextDrawImageW(CGContextRef c,
                        const CGRect *rect,
                        CGImageRef image)
{ CGContextDrawImage(c, *rect, image); }
