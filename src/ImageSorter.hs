{-# LANGUAGE BangPatterns #-}
import Control.Monad
import Data.Binary
import Data.Maybe
import Data.Packed.Matrix
import Data.Packed.Vector(Vector)
import Data.List
import Data.Function
import Graphics.Plot
import Mac.CoreGraphics.Draw.Layout
import Mac.CoreGraphics.Geometry
import Numeric.Container (konst, cmap, vecdisp, dispf, sub, dot)
import Numeric.LinearAlgebra.Algorithms (pinv)
import Options.Applicative
import System.Directory
import System.Environment
import System.FilePath
import System.IO
import qualified Data.Array as Array
import qualified Data.ByteString.Lazy as LBS
import qualified Data.Image.Histogram as Hist
import qualified Data.Map as Map
import qualified Data.Vector as Vector
import qualified Mac.CoreGraphics.Draw as Draw
import qualified Mac.CoreGraphics.Draw.Context as Context
import qualified Mac.CoreGraphics.Image as Image
import qualified Mac.CoreGraphics.ImageDestination as ImgDest
import qualified Mac.CoreGraphics.ImageSource as ImgSrc
import qualified Numeric.Analysis.InvertibleTransform as Trans
import qualified Numeric.Analysis.KMeans as KMeans
import qualified Numeric.Analysis.NMF as NMF
import qualified Numeric.Analysis.PCA as PCA
import qualified Numeric.Analysis.SOM as SOM

type ImageDataDB = Map.Map FilePath (Vector Double)

buildHistograms :: [FilePath] -> IO [(FilePath, Vector Double)]
buildHistograms files = let total = length files in
  forM (zip [1..] files) $ \(i, file) -> do
    putStrLn $ "(" ++ show (i :: Int) ++ "/" ++ show total ++ ") Processing " ++ file ++ "..."
    image <- ImgSrc.imageFromPath file
    pixelArray <- Image.getPixelImage image
    let !histogram = Hist.fromPixelImage 10 pixelArray
    return (file, histogram)

loadFromFile :: (Binary a) => FilePath -> IO a
loadFromFile path = withBinaryFile path ReadMode $ \file -> do
  fileData <- decode <$> LBS.hGetContents file
  return $! fileData

saveToFile :: (Binary a) => FilePath -> a -> IO ()
saveToFile output d = withBinaryFile output WriteMode $ \file -> LBS.hPutStr file (encode d)

toClusters :: (Ord b) => [(a, b)] -> Map.Map b [a]
toClusters assocs = Map.fromListWith (++) $ map (\(v, c) -> (c, [v])) assocs

copyClusters :: (Show a) => FilePath -> Map.Map a [FilePath] -> IO ()
copyClusters dir clusters =
  forM_ (Map.toAscList clusters) $ \(cid, files) -> do
    let clusterDir = dir </> show cid
    createDirectoryIfMissing True clusterDir
    forM_ (zip [0..] files) $ \(fi, file) ->
      copyFile file $ (clusterDir </> show (fi :: Int)) ++ takeExtension file

drawToImage :: FilePath -> Draw.Draw a -> IO a
drawToImage path draw = do
  ctx <- Context.createBitmapContextRGB8 1920 1080
  ret <- Draw.runDraw ctx draw
  img <- Context.createImage ctx
  imgDest <- ImgDest.create path "public.png" 1
  ImgDest.addImage imgDest img
  ImgDest.finalize imgDest
  return ret

bestMatch :: ImageDataDB -> Vector Double -> FilePath
bestMatch db dataPoint = fst $ minimumBy distCompare $ Map.toAscList db
  where distCompare = compare `on` (dist2 . snd)
        dist2 x = let xd = x `sub` dataPoint in xd `dot` xd

updateDB :: FilePath -> [FilePath] -> IO ()
updateDB dbFile files = do
  newData <- Map.fromList <$> buildHistograms files
  fileExists <- doesFileExist dbFile
  outputData <- if fileExists
                  then Map.union newData <$> loadFromFile dbFile
                  else return newData
  saveToFile dbFile outputData

listDB :: FilePath -> IO ()
listDB dbFile = do
  database <- loadFromFile dbFile :: IO ImageDataDB
  forM_ (Map.keys database) putStrLn

makeTransformPCA :: Int -> FilePath -> FilePath -> IO ()
makeTransformPCA n dbFile output = do
  histograms <- Map.elems <$> (loadFromFile dbFile :: IO ImageDataDB)
  let pcas = PCA.pca (fromColumns histograms)
      eigenvectors = takeColumns n $ PCA.eigenvectors pcas
      transform = Trans.InvertibleTransform { Trans.offset = cmap negate $ PCA.dataMean pcas,
                                              Trans.matrix = trans eigenvectors,
                                              Trans.inverseMatrix = eigenvectors}
  saveToFile output transform

makeTransformNMF :: NMF.Params -> FilePath -> FilePath -> IO ()
makeTransformNMF params dbFile output = do
  histograms <- Map.elems <$> (loadFromFile dbFile :: IO ImageDataDB)
  let (wMatrix, _) = NMF.nmf params (fromColumns histograms)
      transform = Trans.InvertibleTransform { Trans.offset = konst 0 $ rows wMatrix,
                                              Trans.matrix = pinv wMatrix,
                                              Trans.inverseMatrix = wMatrix }
  LBS.writeFile output $ encode transform

layoutClusters :: Map.Map a [FilePath] -> Grid (Grid ImagePath)
layoutClusters clusterMap = listToGrid $ map (listToGrid . map ImagePath) $ Map.elems clusterMap

kmeans :: Int -> Int -> Int -> FilePath -> FilePath -> FilePath -> IO ()
kmeans seed maxIter k transformFile dbFile output = do
  transform <- loadFromFile transformFile
  db <- loadFromFile dbFile
  let dataMatrix = fromColumns $ Map.elems db
      tDataMatrix = Trans.matrixApply transform dataMatrix
      dataPoints = Vector.fromList $ toColumns tDataMatrix
      membership = KMeans.kmeans seed maxIter k dataPoints
      clusters = toClusters $ zip (Map.keys db) (Vector.toList membership)

  -- Copy clusters to folder
  -- copyClusters output clusters

  -- Visualize cluster as well
  drawToImage (output </> "vis.png") $ do
    ctxBounds <- fromMaybe emptyRect <$> Draw.getBounds
    drawInRect (layoutClusters clusters) ctxBounds

plotTransform :: Int -> FilePath -> FilePath -> IO ()
plotTransform n input output = do
  transform <- loadFromFile input
  let histograms = take n $ toColumns $ Trans.inverseMatrix transform
  forM_ (zip [0..] histograms) $ \(i, vec) ->
    plotHistogramRGB vec (output ++ "-" ++ show (i :: Int))

profileImage :: FilePath -> FilePath -> IO ()
profileImage transFile imageFile = do
  transform <- loadFromFile transFile
  image <- ImgSrc.imageFromPath imageFile
  pixelArray <- Image.getPixelImage image
  let transformed = Trans.apply transform $ Hist.fromPixelImage 10 pixelArray
  putStrLn $ vecdisp (dispf 4) transformed

buildSom :: Int -> Int -> FilePath -> FilePath -> FilePath -> IO ()
buildSom seed size transformFile dbFile outputFile = do
  transform <- loadFromFile transformFile
  db <- fmap (Trans.apply transform) <$> loadFromFile dbFile
  let initSom = SOM.initialize seed size (Trans.outputComponents transform)
      som = SOM.repeatTrain 100 (SOM.standardFunc 1 10) initSom $ Map.elems db
      grid = SOM.toGrid som :: Grid (Vector Double)
      imageGrid = fmap (ImagePath . bestMatch db) grid
  drawToImage outputFile $ do
    bounds <- fromMaybe emptyRect <$> Draw.getBounds
    drawInRect imageGrid bounds

seedParser :: Parser Int
seedParser = option (short 's' <>
                     long "seed" <>
                     value 1337 <>
                     metavar "SEED" <>
                     help "The seed for the initialization")

maxIterationsParser :: Parser Int
maxIterationsParser = option (short 'i' <>
                              long "iterations" <>
                              value 1000 <>
                              metavar "ITERATIONS" <>
                              help "The maximum number of iterations")

dbFileParser :: Parser FilePath
dbFileParser = argument str (metavar "DB_FILE" <>
                             help "The database file containing histograms")

readNmfVariant :: String -> Either String NMF.Variant
readNmfVariant string | string == "leeseung" = Right NMF.LeeSeung
                      | string == "onmf"     = Right NMF.ONMF
                      | otherwise            = Left "Invalid NMF variant"

nmfParamsParser :: Parser NMF.Params
nmfParamsParser = NMF.Params
                  <$> nullOption (short 'V' <>
                                  long "variant" <>
                                  value NMF.LeeSeung <>
                                  eitherReader readNmfVariant <>
                                  help "The NMF variant (alternatives: leeseung, onmf)")
                  <*> option (short 'k' <>
                              value 12 <>
                              metavar "K" <>
                              help "The number of vectors in the W matrix")
                  <*> maxIterationsParser
                  <*> seedParser

transformFileParser :: Parser FilePath
transformFileParser = strOption (short 't' <>
                                 metavar "TRANSFORM_FILE" <>
                                 help "The transform for reducing dimensions")

opts :: Parser (IO ())
opts = hsubparser
       (command "updatedb" (info (updateDB
                                 <$> strOption (short 'o' <>
                                                long "output" <>
                                                help "The output database file")
                                 <*> many (argument str (metavar "IMAGE_FILES ..." <>
                                                         help "The image files to process")))
                             (progDesc "Update/create a database with data for the given image files")) <>
        command "listdb" (info (listDB <$> dbFileParser)
                          (progDesc "List files in the given database")) <>
        command "makepca" (info (makeTransformPCA
                                 <$> option (short 'n' <>
                                             value 10 <>
                                             metavar "N" <>
                                             help "The number of eigenvectors to keep")
                                 <*> dbFileParser
                                 <*> argument str (metavar "OUTPUT" <>
                                                   help "Output file"))
                           (progDesc "Create PCA profile for given images")) <>
        command "plottransform" (info (plotTransform
                                       <$> option (short 'n' <>
                                                   value 20 <>
                                                   metavar "N" <>
                                                   help "The number of columns to plot")
                                       <*> argument str (metavar "PROFILE" <>
                                                         help "The transform file")
                                       <*> argument str (metavar "OUTPUT_PREFIX" <>
                                                         help "The prefix of the output file(s)"))
                                 (progDesc "Plot columns from the inverse matrix of a transform")) <>
        command "makenmf" (info (makeTransformNMF
                                 <$> nmfParamsParser
                                 <*> dbFileParser
                                 <*> argument str (metavar "OUTPUT" <>
                                                   help "Output file"))
                           (progDesc "Create a transform using NMF")) <>
        command "kmeans" (info (kmeans
                                <$> seedParser
                                <*> maxIterationsParser
                                <*> option (short 'k' <>
                                            value 20 <>
                                            metavar "K" <>
                                            help "The number of clusters")
                                <*> transformFileParser
                                <*> dbFileParser
                                <*> argument str (metavar "OUTPUT_DIR" <>
                                                  help "Output directory"))
                          (progDesc "Create NMF profile for given images")) <>
        command "profileimage" (info (profileImage
                                      <$> argument str (metavar "TRANSFORM_FILE" <>
                                                        help "The transform file")
                                      <*> argument str (metavar "IMAGE_FILE" <>
                                                        help "The image file"))
                                (progDesc "Prints the transformed histogram for the given image")) <>
        command "som" (info (buildSom
                             <$> seedParser
                             <*> option (short 's' <>
                                         value 20 <>
                                         metavar "SIZE" <>
                                         help "The size of the SOM")
                             <*> transformFileParser
                             <*> dbFileParser
                             <*> argument str (metavar "OUTPUT_FILE" <>
                                               help "The output image file"))
                       (progDesc "Builds a self-organizing map")))

main :: IO ()
main = join $ execParser parserInfo
  where parserInfo = info (helper <*> opts)
                     (fullDesc <>
                      header "Spectral image analysis tool")
