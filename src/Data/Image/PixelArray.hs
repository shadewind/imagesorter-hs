{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-|
Module     : Data.Image.PixelArray
Maintainer : Emil Eriksson <shadewind@gmail.com>

Pixel array types based on type families.
-}

module Data.Image.PixelArray (
  PixelBuffer(..),
  PixelArray(..),
  PixelConvertible(..),
  RGB8(..),
  RGBF(..),
  
  coordinates,
  pixels,
  foldlPixels,
  foldrPixels,
  foldlPixels',
  mapPixelsM_,
  forPixelsM_
  ) where

import Data.Word
import qualified Data.Vector.Storable as V

-- |Data family for in memory pixel images.
data family PixelBuffer p

-- |Typeclass for pixels.
class Pixel p

-- |Typeclass for pixels
class PixelArray arr where
  -- |The pixel type of the array.
  type PixelType px
  -- |Returns the pixel at the given coordinates
  unsafePixelAt :: arr -> (Int, Int) -> PixelType arr
  -- |Returns the image width in pixels
  width :: arr -> Int
  -- |Returns the image height in pixels
  height :: arr -> Int

-- |Class for declaring something convertible to something other
class (Pixel a, Pixel b) => PixelConvertible a b where
  -- |Converts the given pixel to another type
  convertPixel :: a -> b
  
instance (Pixel a) => PixelConvertible a a where
  convertPixel = id

-- |Returns a list of the coordinates of the given pixel array from left to right,
-- top to bottom.
coordinates :: (PixelArray arr) => arr -> [(Int, Int)]
{-# INLINE coordinates #-}
coordinates arr = [(x, y) | y <- [0..(height arr - 1)], x <- [0..(width arr - 1)]]

-- |Returns a list of all the coordinates in the given PixelArray from left to right,
-- top to bottom.
pixels :: (PixelArray arr) => arr -> [PixelType arr]
{-# INLINE pixels #-}
pixels arr = map (unsafePixelAt arr) $ coordinates arr
      
-- |Fold over pixels, left to right, top to bottom
foldlPixels :: (PixelArray arr) => (a -> PixelType arr -> a) -> a -> arr -> a
{-# INLINE foldlPixels #-}
foldlPixels f iacc arr = foldl f iacc (pixels arr)

-- |Fold over pixels, right to left, bottom to top
foldrPixels :: (PixelArray arr) => (PixelType arr -> a -> a) -> a -> arr -> a
{-# INLINE foldrPixels #-}
foldrPixels f iacc arr = foldr f iacc (pixels arr)

-- |Strict fold over pixels, left to right, top to bottom
foldlPixels' :: (PixelArray arr) => (a -> PixelType arr -> a) -> a -> arr -> a
{-# INLINE foldlPixels' #-}
foldlPixels' f iacc arr = foldl f iacc (pixels arr)

-- |Monadic map over pixels ignoring the result
mapPixelsM_ :: (Monad m, PixelArray arr) => (PixelType arr -> m a) -> arr -> m ()
{-# INLINE mapPixelsM_ #-}
mapPixelsM_ f arr = foldrPixels f' (return ()) arr
  where f' p accM = f p >> accM

-- |Monadic map over pixels ignoring the result
forPixelsM_ :: (Monad m, PixelArray arr) => arr -> (PixelType arr -> m a) -> m ()
{-# INLINE forPixelsM_ #-}
forPixelsM_ = flip mapPixelsM_

-- |RGB pixel with 8-bit components
data RGB8 = RGB8 {-# UNPACK #-} !Word8 {-# UNPACK #-} !Word8 {-# UNPACK #-} !Word8
          deriving (Eq, Show)

instance PixelConvertible RGBF RGB8 where
  {-# INLINE convertPixel #-}
  convertPixel (RGBF rf gf bf) = RGB8 (to8 rf) (to8 gf) (to8 bf)
    where to8 x = round $ (clamp 0.0 1.0 x) * 255.0
          clamp mi ma v = max mi (min ma v)

instance Pixel RGB8

data instance PixelBuffer RGB8 =
  PixelBufferRGB8 { rgb8Width :: !Int,
                   rgb8Height :: !Int,
                   rgb8RowStride :: !Int,
                   rgb8PixelStride :: !Int,
                   rgb8IndexRed :: !Int,
                   rgb8IndexGreen :: !Int,
                   rgb8IndexBlue :: !Int,
                   rgb8Data :: !(V.Vector Word8) }
  deriving (Eq, Show)

instance PixelArray (PixelBuffer RGB8) where
  type PixelType (PixelBuffer RGB8) = RGB8

  {-# INLINE unsafePixelAt #-}
  unsafePixelAt img (x, y) = RGB8 r g b
    where r = (V.!) (rgb8Data img) $ offset + (rgb8IndexRed img)
          g = (V.!) (rgb8Data img) $ offset + (rgb8IndexGreen img)
          b = (V.!) (rgb8Data img) $ offset + (rgb8IndexBlue img)
          offset = (rgb8RowStride img) * y + (rgb8PixelStride img) * x

  {-# INLINE width #-}
  width = rgb8Width
  {-# INLINE height #-}
  height = rgb8Height

-- |RGB pixel with 'Float' components
data RGBF = RGBF {-# UNPACK #-} !Float {-# UNPACK #-} !Float {-# UNPACK #-} !Float
          deriving (Eq, Show)

instance Pixel RGBF
                   
instance PixelConvertible RGB8 RGBF where
  {-# INLINE convertPixel #-}
  convertPixel (RGB8 r8 g8 b8) = RGBF (toF r8) (toF g8) (toF b8)
    where toF x = fromIntegral x / 255.0
