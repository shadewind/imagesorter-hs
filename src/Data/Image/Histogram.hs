{-# LANGUAGE FlexibleContexts #-}
{-|
Module     : Data.Image.Histogram
Maintainer : Emil Eriksson <shadewind@gmail.com>

Misc image algorithms.
-}

module Data.Image.Histogram (
  binForPixel,
  pixelForBin,
  fromPixelImage,
  ) where

import Control.Monad.ST
import Data.Image.PixelArray
import Numeric.Container
import qualified Data.Vector.Storable as V
import qualified Data.Vector.Storable.Mutable as MV

-- |Returns the bin index of the given pixel for a given resolution
binForPixel :: Int  -- ^The resolution of the histogram
            -> RGBF -- ^The pixel
            -> Int  -- ^The bin index
{-# INLINE binForPixel #-}
binForPixel res (RGBF r g b) = (res * res * binc r) + (res * binc g) + binc b
  where binc x = min (res - 1) $ truncate $ fromIntegral res * x

-- |Returns the color of the pixel for the given bin index for the given resolution.
-- The color will be centered on the bin.
pixelForBin :: Int  -- ^The resolution of the histogram
            -> Int  -- ^The bin index
            -> RGBF -- ^The centered color
pixelForBin res index = RGBF (toF ri) (toF gi) (toF bi)
  where toF x = (fromIntegral x + 0.5) / fromIntegral res
        ri = index `quot` (res * res)
        gi = (index `quot` res) `rem` res
        bi = index `rem` res

-- |Creates an RGB histogram from the given 'PixelArray'. The histogram will have
-- 'resolution ^ 3' bins. The bin index can be converted to an from color using
-- 'binForPixel' and 'pixelForBin'.
fromPixelImage :: (PixelArray arr, PixelConvertible (PixelType arr) RGBF)
                  => Int             -- ^The resolution as described above
                  -> arr             -- ^A pixel array
                  -> Vector Double -- ^A vector of the histogram bins
{-# SPECIALIZE fromPixelImage :: Int -> PixelBuffer RGB8 -> Vector Double #-}
fromPixelImage res arr = V.map normalize (rawHist :: Vector Int)
  where normalize x = fromIntegral x / fromIntegral nPixels
        nPixels = width arr * height arr
        rawHist = runST $ do
          hist <- MV.replicate (res * res * res) 0
          forPixelsM_ arr $ \px -> do
            let index = binForPixel res $ convertPixel px
            n <- MV.read hist index
            MV.write hist index (n + 1)
          V.freeze hist
