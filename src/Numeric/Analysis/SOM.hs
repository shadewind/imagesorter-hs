{-# LANGUAGE FlexibleContexts #-}
{-|
Module     : Numeric.Analysis.SOM
Maintainer : Emil Eriksson <shadewind@gmail.com>

Self-organizing maps
-}

module Numeric.Analysis.SOM (
  SOM(..),
  Index,
  DecayFunc,
  standardFunc,
  bmu,
  trainPoint,
  trainPoints,
  repeatTrain,
  initialize,
  toGrid
  ) where

import Data.Array.IArray
import Data.Function
import Data.List
import Data.Packed.Vector
import Data.Tuple
import Numeric.Container (add, sub, scale, dot, randomVector, RandDist(..))
import System.Random
import qualified Data.Vector as V
import Control.DeepSeq
import Debug.Trace

-- |Self-organizing map consisting of the map size and a vector
-- containing the data.
data SOM = SOM {
  -- |The size of the grid
  size :: !Int,
  -- |The elements of the grid laid out in row-major order
  nodes :: !(V.Vector (Vector Double)),
  -- |The number of learning iterations taken by this 'SOM'
  numSteps :: !Int
  }
           
-- |Index of a SOM element.
type Index = Int

-- |A decay function. The decay coefficient is a function of the distance between the particular
-- element and the best matching unit as well as the step index.
type DecayFunc = Int -> Double -> Double

-- |Standard learning function with gaussian distance falloff and inverse
-- linear step falloff.
standardFunc
  :: Double     -- ^The steepness of learning falloff across steps
  -> Double     -- ^The width of the distance falloff
  -> DecayFunc  -- ^The learning function
standardFunc s d step dist = stepFalloff * distFalloff
  where stepFalloff = 1 / (s * stepf)
        distFalloff = exp $ -(stepf * dist * dist) / (2 * d * d)
        stepf = fromIntegral $ step + 1

-- |Returns the best matching unit for the given data point.
bmu :: SOM -> Vector Double -> Index
bmu (SOM _ elements _) point = V.minIndexBy (compare `on` dist2) elements
  where dist2 e = let x = point `sub` e in x `dot` x

-- |Returns the distance between the nodes with the given indexes for a SOM
-- of a given size.
distance
  :: Int    -- ^The size of the SOM.
  -> Int    -- ^The first index
  -> Int    -- ^The second index
  -> Double -- ^The euclidean distance
distance sz a b = sqrt $ (dx * dx) + (dy * dy)
  where dx = fromIntegral $ (a `rem` sz) - (b `rem` sz)
        dy = fromIntegral $ (a `quot` sz) - (b `quot` sz)

-- |Updates the given 'SOM' using the given data point.
trainPoint
  :: DecayFunc     -- ^The decay function
  -> SOM           -- ^The input SOM
  -> Vector Double -- ^The data point to learn
  -> SOM           -- ^The output SOM
trainPoint df som target = 
  som { nodes = force $ V.imap learn (nodes som) }
  where learn i p = let coeff = df (numSteps som) (distance (size som) i bmui)
                    in p `add` (coeff `scale` (target `sub` p))
        bmui = bmu som target

-- |Updates the given 'SOM' using a list of data points. The step counter is increased.
trainPoints
  :: DecayFunc       -- ^The decay function
  -> SOM             -- ^The input SOM
  -> [Vector Double] -- ^The data points to learn
  -> SOM             -- ^The output SOM
trainPoints df som points = let som' = foldl' (trainPoint df) som points
                            in som' { numSteps = numSteps som + 1 }

-- |Repeatedly applies 'trainPoints' to the given SOM
repeatTrain
  :: Int
  -> DecayFunc
  -> SOM
  -> [Vector Double]
  -> SOM
repeatTrain 0 _ som _ = som
repeatTrain n df som points = repeatTrain (n - 1) df trainedSom points
  where trainedSom = trainPoints df som points

-- |Randomly initializes a SOM of the given size and node dimensions.
initialize
  :: Int -- ^The seed
  -> Int -- ^The size of the SOM
  -> Int -- ^The dimensionality of the nodes
  -> SOM
initialize seed sz d = SOM sz initNodes 0
  where initNodes = V.fromList $ map makeVector seeds
        makeVector s = randomVector s Uniform d
        seeds = take (sz * sz) $ randoms (mkStdGen seed)

-- |Converts the given 'SOM' to a two dimensional array of the nodes.
toGrid :: (IArray a (Vector Double)) => SOM -> a (Int, Int) (Vector Double)
toGrid (SOM sz nodeVector _) = array arrayRange nodeAssocs
  where arrayRange = ((0,0), (sz - 1, sz - 1))
        nodeAssocs = zip (map swap $ range arrayRange) $ V.toList nodeVector
