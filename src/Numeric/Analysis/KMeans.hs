{-|
Module     : Numeric.Analysis.KMeans
Maintainer : Emil Eriksson <shadewind@gmail.com>

K-means clustering functions
-}

module Numeric.Analysis.KMeans (
  kmeans
  ) where

import Data.Packed.Vector
import Numeric.Container
import System.Random
import Data.Function
import Control.Monad.ST
import qualified Data.Vector as V
import qualified Data.Vector.Mutable as MV
import Debug.Trace
import Control.Applicative
import Data.Maybe

-- |Performs K-means clustering on the given set of data points. The result is
-- returned as a vector of integers describing the cluster member of the corresponding
-- data point.
kmeans :: Int                      -- ^The seed for the initialization
       -> Int                      -- ^The maximum number of iterations
       -> Int                      -- ^The number of clusters
       -> V.Vector (Vector Double) -- ^The data points
       -> V.Vector Int             -- ^The cluster membership vector
kmeans seed maxIter k dataPoints = kmeans' maxIter initialCentroids initialMembership
  where initialCentroids = initCentroids seed k dataPoints
        initialMembership = assign initialCentroids dataPoints
        kmeans' n centroids membership =
          let membership' = assign centroids dataPoints
              maybeCentroids = toCentroids k dataPoints membership'
              centroids' = fromJust <$> V.zipWith (<|>) maybeCentroids (fmap Just centroids)
          in if (n > 0) && (membership /= membership')
               then kmeans' (n - 1) centroids' (traceShow membership membership')
               else membership'

-- |Randomly initializes cluster centroids
initCentroids :: Int -> Int -> V.Vector (Vector Double) -> V.Vector (Vector Double)
initCentroids seed k dataPoints = V.fromList centroidList
  where centroidList = map (dataPoints V.!) randomIndexes
        randomIndexes = take k $ randomRs (0, V.length dataPoints - 1) rnd
        rnd = mkStdGen seed

-- |Returns the index of the centroid closest to the given vector
nearestCentroid :: V.Vector (Vector Double) -> Vector Double -> Int
nearestCentroid centroids v = V.minIndexBy (compare `on` dist2) centroids
  where dist2 c = let x = c `sub` v in x `dot` x

-- |Returns a vector containing the indexes of the cluster closest to each vector
assign :: V.Vector (Vector Double) -> V.Vector (Vector Double) -> V.Vector Int
assign centroids = V.map (nearestCentroid centroids)

data VectorSum = VectorSum !Int !(Vector Double)

toCentroids :: Int
            -> V.Vector (Vector Double)
            -> V.Vector Int
            -> V.Vector (Maybe (Vector Double))
toCentroids k dataPoints memberships = runST $ do
  let dimensions = dim (V.head dataPoints)
  sums <- MV.replicate k (VectorSum 0 (konst 0 dimensions))
  V.forM_ (V.zip memberships dataPoints) $ \(clusterIndex, vec) -> do
    s <- MV.read sums clusterIndex
    MV.write sums clusterIndex (addToSum s vec)
  fsums <- V.unsafeFreeze sums
  return $ V.map sumToVec fsums
  where addToSum (VectorSum n s) v = VectorSum (n + 1) (s `add` v)
        sumToVec (VectorSum n s) = if n == 0
                                     then Nothing
                                     else Just $ cmap (/ fromIntegral n) s
