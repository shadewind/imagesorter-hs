{-# LANGUAGE FlexibleContexts #-}
{-|
Module     : Numeric.Analysis.PCA
Maintainer : Emil Eriksson <shadewind@gmail.com>

PCA analysis functions.
-}

module Numeric.Analysis.PCA (
  PCA(..), pca
  ) where

import Data.Binary
import Data.Packed.Vector
import Data.Packed.Matrix
import Control.Applicative
import Numeric.Container
import Numeric.LinearAlgebra.Algorithms

-- |Result of PCA analysis.
data PCA = PCA { dataMean :: !(Vector Double),
                 eigenvalues :: !(Vector Double),
                 eigenvectors :: !(Matrix Double) }

instance Binary PCA where
  put (PCA mean eigval eigvec) = do
    put mean
    put eigval
    put eigvec

  get = PCA <$> get <*> get <*> get

-- |Performs PCA on the given matrix. The columns are interpreted as data points.
-- The result is returned as a triple of mean, eigenvalues and eigenvectors respectively.
pca :: Matrix Double -> PCA
pca matrix =
  let n = cols matrix
      mean = matrix `mXv` konst (recip $ fromIntegral n) n
      centered = matrix `sub` (mean `outer` konst 1 n)
      (eigval, eigvec) = eigSH $ trans centered <> centered
  in PCA mean eigval (centered <> eigvec) -- TODO in some cases, there may be more data points than components
