{-|
Module     : Numeric.Analysis.NMF
Maintainer : Emil Eriksson <shadewind@gmail.com>

Non-negative matrix factorization implementation
-}

module Numeric.Analysis.NMF (
  Variant(..),
  Params(..),
  nmf
  ) where

import Data.Packed.Matrix
import Numeric.Container
import Debug.Trace

-- |The NMF algorithm variant.
data Variant = LeeSeung -- ^Multiplicative updates (Lee & Seung, 1999)
             | ONMF     -- ^Modification for increased orthogonality (Yoo & Choi, 2010)

-- |Parameters for NMF algorithm.
data Params = Params {
  -- |The NMF variant
  variant :: !Variant,
  -- |Number of columns in matrix W
  kValue :: !Int,
  -- |The maximum number of iterations
  maxIterations :: !Int,
  -- |The seed for the initialization
  seedValue :: !Int
  }

-- |Creates a random matrix of the given dimensions
randomMatrix :: Int -> (Int, Int) -> Matrix Double
randomMatrix seed (r, c) = reshape c $ randomVector seed Uniform (r * c)

-- |Performs NMF on the given matrix with the given parameters. Returns the component and weight
-- matrices as a tuple '(w, h)'
nmf :: Params -> Matrix Double -> (Matrix Double, Matrix Double)
nmf params v = let (Triple _ w h) = case variant params of
                     LeeSeung -> nmfWith [stepNormW, stepNormH] params v
                     ONMF -> nmfWith [stepNormW, stepNormOrthoH] params v
               in (w, h)

-- |Triple of NMF matrices: 'Triple v w h'
data Triple = Triple !(Matrix Double) !(Matrix Double) !(Matrix Double)
-- |Type of iteration function taking an 'Triple' and yielding a new 'Triple'
type StepFunc = (Triple -> Triple)

-- |Performs NMF with the given list of iteration function and params
nmfWith :: [StepFunc] -> Params -> Matrix Double -> Triple
nmfWith steps params v = iterateNMF iterations params (initNMF params v)
  where iterations = take (maxIterations params) $ cycle steps

-- |Iterates using the given step functions and the given parameters
iterateNMF :: [StepFunc] -> Params -> Triple -> Triple
iterateNMF funcs _ initTriple = iterateNMF' funcs initTriple (0 :: Int) (nmfLoss initTriple)
  where iterateNMF' [] triple _ _ = triple
        iterateNMF' (f:xs) triple n loss =
          let triple' = f triple
              loss' = nmfLoss triple'
          in if (n `rem` 50) == 0 && loss' > loss -- Stop when it gets worse
               then triple
               else iterateNMF' xs triple' (n + 1) loss'

-- |Initializes an NMF triple from the given parameters and data matrix
initNMF :: Params -> Matrix Double -> Triple
initNMF params v = Triple v' w h
  where w = randomMatrix (seedValue params) (rows v, kValue params)
        h = randomMatrix (seedValue params + 1) (kValue params, cols v)
        v' = cmap (+ 1e-40) v

-- |Calculates the loss of the approximation given by the triple using the norm of the
-- difference
nmfLoss :: Triple -> Double
nmfLoss (Triple v w h) = let l = norm2 $ flatten $ v `sub` (w `mXm` h)
                    in traceShow l l

stepNormW :: Triple -> Triple
stepNormW (Triple v w h) = Triple v w' h
  where w' = w `mul` ((v `mXm` trans h) `divide` optimiseMult [w, h, trans h])

stepNormH :: Triple -> Triple
stepNormH (Triple v w h) = Triple v w h'
  where h' = h `mul` ((trans w `mXm` v) `divide` optimiseMult [trans w, w, h])
            
stepNormOrthoH :: Triple -> Triple
stepNormOrthoH (Triple v w h) = Triple v w (h' `mXm` dh)
  where h' = h `mul` ((trans w `mXm` v) `divide` optimiseMult [h, trans v, w, h])
        dh = diag $ cmap (1 /) $ konst 1 (rows h') `vXm` h'
