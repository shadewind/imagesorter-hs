{-|
Module     : Numeric.Analysis.InvertibleTransform
Maintainer : Emil Eriksson <shadewind@gmail.com>

Pseudo-invertible vector transforms
-}

module Numeric.Analysis.InvertibleTransform (
  InvertibleTransform(..),
  inputComponents,
  outputComponents,
  apply, matrixApply
  ) where

import Data.Packed.Vector
import Data.Packed.Matrix
import Numeric.Container
import Data.Binary
import Control.Applicative

-- |A vector transformation which can be inverted although not necessarily
-- lossless.
data InvertibleTransform = InvertibleTransform {
  -- |The offset to apply to the vector before multiplication with the
  -- transformation matrix
  offset :: !(Vector Double),
  -- |The transformation matrix
  matrix :: !(Matrix Double),
  -- |The inverse transformation matrix
  inverseMatrix :: !(Matrix Double)
  }

instance Binary InvertibleTransform where
  put t = do
    put $ offset t
    put $ matrix t
    put $ inverseMatrix t

  get = InvertibleTransform <$> get <*> get <*> get

-- |Returns the number of input components for the transform
inputComponents :: InvertibleTransform -> Int
inputComponents t = cols $ matrix t

-- |Returns the number of output components for the transform
outputComponents :: InvertibleTransform -> Int
outputComponents t = rows $ matrix t

-- |Applies the given transform to the given vector
apply :: InvertibleTransform -> Vector Double -> Vector Double
apply t v = matrix t `mXv` (v `add` offset t)

-- |Applies the given transform to the given matrix
matrixApply :: InvertibleTransform -> Matrix Double -> Matrix Double
matrixApply t m = matrix t `mXm` (m `add` (offset t `outer` konst 1 (cols m)))
