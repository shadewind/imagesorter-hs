{-|
Module     : Graphics.Plot
Maintainer : Emil Eriksson <shadewind@gmail.com>

Plotting with gnuplot.
-}

module Graphics.Plot (
  gnuplot,
  plotHistogramRGB,
  plotPosHistogramRGB,
  plotMixedSignHistogramRGB
  ) where

import Control.Monad
import Data.Image.PixelArray
import Numeric.Container
import System.IO
import System.Process
import qualified Data.Image.Histogram as Hist
import qualified Data.Vector.Storable as V

-- |Takes a function which is passed another function which when called runs the given
-- command in gnuplot
gnuplot :: ((String -> IO ()) -> IO ()) -> IO ()
gnuplot f = do
  (gin, _, _, _) <- runInteractiveCommand "gnuplot"
  f (hPutStrLn gin)
  hClose gin

-- |Plots the given histogram. The histogram is split into negative and positive parts
-- if it contains negative values.
plotHistogramRGB :: Vector Double -> FilePath -> IO ()
plotHistogramRGB hist | minElement hist >= 0.0 = plotPosHistogramRGB hist
                      | otherwise = plotMixedSignHistogramRGB hist

-- |Plots the given RGB histogram to the given file which is assumed to have only
-- positive values
plotPosHistogramRGB :: Vector Double -> FilePath -> IO ()
plotPosHistogramRGB hist path = do
  let resolution = round $ fromIntegral (V.length hist) ** (recip 3 :: Double)
      normHist = scale (recip $ maxElement hist) hist
  gnuplot $ \p -> do
    p $ "set terminal svg"
    p $ "set output '" ++ path ++ ".svg'"
    p $ "set xlabel 'Red' tc rgb 'red'"
    p $ "set xrange [0:255]"
    p $ "set ylabel 'Green' tc rgb 'green'"
    p $ "set yrange [0:255]"
    p $ "set zlabel 'Blue' tc rgb 'blue'"
    p $ "set zrange [0:255]"
    p $ "set pointsize 2.0"
    p $ "rgb(r,g,b) = int(r)*65536 + int(g)*256 + int(b)"
    p $ "splot '-' using 1:2:3:4:(rgb($1,$2,$3)) title \"RGB histogram\" with points pt 7 ps variable lc rgb variable"
    forM_ (zip [0..] (V.toList normHist)) $ \(i, v) -> do
      let (RGB8 r g b) = convertPixel $ Hist.pixelForBin resolution i
      p $ unwords [show r, show g, show b, show (1 - 1 / (10 * v + 1))]

-- |Plots the given RGB histogram as positive and negative parts
plotMixedSignHistogramRGB :: Vector Double -> FilePath -> IO ()
plotMixedSignHistogramRGB eig path = do
  let pos = cmap (max 0.0) eig
      neg = cmap (max 0.0 . negate) eig
  plotPosHistogramRGB pos (path ++ "-pos")
  plotPosHistogramRGB neg (path ++ "-neg")
