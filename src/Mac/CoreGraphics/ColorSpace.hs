{-|
Module     : Mac.CoreGraphics.ColorSpace
Maintainer : Emil Eriksson <shadewind@gmail.com>

Wrapper around CGColorSpace
-}

module Mac.CoreGraphics.ColorSpace (
  ColorSpace,
  Model(..),
  createDeviceRGB,
  getModel,
  getNumberOfComponents,
  withColorSpace,
  unsafeCreate
  ) where

import Mac.CoreFoundation.Raw
import Mac.CoreFoundation.Utils
import Mac.CoreGraphics.Raw
import Foreign.ForeignPtr
import Control.Applicative

-- |Wrapper for 'CGColorSpace'
newtype ColorSpace = ColorSpace (ForeignPtr CFType)

-- |Describes the color space model
data Model = Monochrome
           | RGB
           | CMYK
           | Lab
           | DeviceNative
           | Indexed
           | Pattern
           deriving (Eq, Show)

-- |Creates a device RGB 'ColorSpace'
createDeviceRGB :: IO ColorSpace
createDeviceRGB = cgColorSpaceCreateDeviceRGB >>= unsafeCreate

-- |Gets the model of the given 'ColorSpace' if it is known
getModel :: ColorSpace -> IO (Maybe Model)
getModel cspace = withColorSpace cspace $ \csref -> do
  nativeModel <- cgColorSpaceGetModel csref
  return $ lookup nativeModel table
  where table = [(kCGColorSpaceModelMonochrome, Monochrome),
                 (kCGColorSpaceModelRGB, RGB),
                 (kCGColorSpaceModelCMYK, CMYK),
                 (kCGColorSpaceModelLab, Lab),
                 (kCGColorSpaceModelDeviceN, DeviceNative),
                 (kCGColorSpaceModelIndexed, Indexed),
                 (kCGColorSpaceModelPattern, Pattern)]

-- |Gets the number of components of the given 'ColorSpace'
getNumberOfComponents :: ColorSpace -> IO Int
getNumberOfComponents cspace = withColorSpace cspace $ \csref ->
  fromIntegral <$> cgColorSpaceGetNumberOfComponents csref

-- |Creates a 'ColorSpace' from the given raw 'CGColorSpaceRef'. The 'CGColorSpaceRef' must not
-- be used afterwards.
unsafeCreate :: CGColorSpaceRef -> IO ColorSpace
unsafeCreate csref = ColorSpace <$> toForeignPtr csref

-- |Passes the underlying 'CGColorSpaceRef' to the given function and returns the action.
withColorSpace :: ColorSpace -> (CGColorSpaceRef -> IO a) -> IO a
withColorSpace (ColorSpace fptr) = withForeignPtr fptr
