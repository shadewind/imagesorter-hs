module Mac.CoreGraphics.Image (
  AlphaInfo(..),
  AlphaType(..),
  PixelType(..),
  ImageInfo(..),
  Image,
  AnyImageArray(..),
  BitmapInfo(..),

  nativeToBitmapInfo,
  bitmapInfoToNative,
  imageInfo,
  getPixelImage,
  getColorSpace,
  unsafeCreate,
  withImage
  ) where

import Control.Applicative
import Control.Monad
import Data.Bits
import Data.Maybe
import Data.Tuple
import Foreign.C
import Foreign.ForeignPtr
import Foreign.Marshal.Utils
import Mac.CoreFoundation.Raw
import Mac.CoreFoundation.Utils
import Mac.CoreGraphics.Raw
import System.IO.Unsafe
import qualified Data.Image.PixelArray as PArray
import qualified Data.Vector.Storable as V
import qualified Mac.CoreGraphics.ColorSpace as CSpace

-- |Wrapper for CGImage
newtype Image = Image (ForeignPtr CFType)

-- |Represents an image of any type supported by Core Graphics
data AnyImageArray = TODO

-- |Information about a CoreGraphics bitmap
data BitmapInfo = BitmapInfo {
  -- |The alpha channel information
  alphaInfo :: !AlphaInfo,
  -- |The type of pixel
  pixelType :: !PixelType
} deriving (Eq, Show)

-- |Information about the alpha channel of a bitmap
data AlphaInfo = First AlphaType -- ^The alpha channel is the first component
               | Last AlphaType  -- ^The alpha channel is the last component
               | None            -- ^There is no alpha channel
               deriving (Eq, Show)

-- |Describes the type of an alpha channel
data AlphaType = Regular        -- ^Regular non-premultiplied
               | Premultiplied  -- ^Pre-multiplied
               | Skipped        -- ^Skipped, ignore the alpha channel
               deriving (Eq, Show)

-- |The binary layout of a pixel in CoreGraphics
data PixelType = LE16               -- ^16-bit little endian
               | LE32               -- ^32-bit little endian
               | BE16               -- ^16-bit big endian
               | BE32               -- ^32-bit big endian
               | Default            -- ^Default layout
               | FloatComponents    -- ^32-bit float components
               deriving (Eq, Show)
  
-- |The properties of an 'Image'
data ImageInfo = ImageInfo {
  -- |The image width in pixels
  imageWidth :: !Int,
  -- |The image height in pixels
  imageHeight :: !Int,
  -- |The number of pixel components
  imageNumComponents :: !Int,
  -- |The image stride in bytes
  imageStride :: !Int,
  -- |The number of bits per pixel
  imageBpp :: !Int,
  -- |The number of bits per component
  imageBpc :: !Int,
  -- |The color model if applicable
  imageColorModel :: !(Maybe CSpace.Model),
  -- |The bitmap info
  imageBitmapInfo :: !BitmapInfo
  } deriving (Eq, Show)

alphaInfoTable :: [(CGBitmapInfo, AlphaInfo)]
alphaInfoTable = [(kCGImageAlphaNone,               None),
                  (kCGImageAlphaPremultipliedLast,  Last Premultiplied),
                  (kCGImageAlphaPremultipliedFirst, First Premultiplied),
                  (kCGImageAlphaLast,               Last Regular),
                  (kCGImageAlphaFirst,              First Regular),
                  (kCGImageAlphaNoneSkipLast,       Last Skipped),
                  (kCGImageAlphaNoneSkipFirst,      First Skipped)]

byteOrderTable :: [(CGBitmapInfo, PixelType)]
byteOrderTable = [(kCGBitmapByteOrderDefault,  Default),
                  (kCGBitmapByteOrder16Little, LE16),
                  (kCGBitmapByteOrder32Little, LE32),
                  (kCGBitmapByteOrder16Big,    BE16),
                  (kCGBitmapByteOrder32Big,    BE32)]

nativeToAlphaInfo :: CGBitmapInfo -> AlphaInfo
nativeToAlphaInfo cgbi = fromMaybe None $ lookup (cgbi .&. kCGBitmapAlphaInfoMask) alphaInfoTable

nativeToPixelType :: CGBitmapInfo -> PixelType
nativeToPixelType cgbi | (cgbi .&. kCGBitmapFloatComponents) /= 0 = FloatComponents
                       | otherwise = fromMaybe Default maybeOrder
  where maybeOrder = lookup (cgbi .&. kCGBitmapByteOrderMask) byteOrderTable
          
-- |Converts from 'CGBitmapInfo' to 'BitmapInfo'
nativeToBitmapInfo :: CGBitmapInfo -> BitmapInfo
nativeToBitmapInfo cgbi = BitmapInfo (nativeToAlphaInfo cgbi) (nativeToPixelType cgbi)

-- |Converts from 'BitmapInfo' to 'CGBitmapInfo'
bitmapInfoToNative :: BitmapInfo -> CGBitmapInfo
bitmapInfoToNative (BitmapInfo ai pt) = aiToNative ai .|. ptToNative pt
  where aiToNative v = fromMaybe kCGImageAlphaNone $ lookup v $ map swap alphaInfoTable
        
        ptToNative FloatComponents = kCGBitmapFloatComponents
        ptToNative v = fromMaybe kCGBitmapByteOrderDefault $ lookup v $ map swap byteOrderTable

-- |Returns the 'ImageInfo' for the given 'Image'
imageInfo :: Image -> ImageInfo
imageInfo img = unsafePerformIO $ withImage img $ \imgref -> do
  imgWidth <- cgImageGetWidth imgref
  imgHeight <- cgImageGetHeight imgref
  stride <- cgImageGetBytesPerRow imgref
  bpp <- cgImageGetBitsPerPixel imgref
  bpc <- cgImageGetBitsPerComponent imgref
  bitmapInfo <- cgImageGetBitmapInfo imgref
  colorSpace <- getColorSpace img
  nComponents <- CSpace.getNumberOfComponents colorSpace
  colorModel <- CSpace.getModel colorSpace
  return ImageInfo { imageWidth = fromIntegral imgWidth,
                     imageHeight = fromIntegral imgHeight,
                     imageStride = fromIntegral stride,
                     imageNumComponents = fromIntegral nComponents,
                     imageBpp = fromIntegral bpp,
                     imageBpc = fromIntegral bpc,
                     imageColorModel = colorModel,
                     imageBitmapInfo = nativeToBitmapInfo bitmapInfo }

imageVector :: Image -> IO (V.Vector CUChar)
imageVector img = withImage img $ \imgref -> do
  provider <- cgImageGetDataProvider imgref
  withCFType (cgDataProviderCopyData provider) $ \cfData -> do
    cfBytePtr <- cfDataGetBytePtr cfData
    size <- fromIntegral <$> cfDataGetLength cfData
    bytePtr <- mallocForeignPtrBytes size
    withForeignPtr bytePtr $ \ptr -> copyBytes ptr cfBytePtr size
    return $ V.unsafeFromForeignPtr0 bytePtr size

getPixelImage :: Image -> IO (PArray.PixelBuffer PArray.RGB8)
getPixelImage img = do
  vec <- V.unsafeCast <$> imageVector img
  return PArray.PixelBufferRGB8 { PArray.rgb8Width = imageWidth i,
                                  PArray.rgb8Height = imageHeight i,
                                  PArray.rgb8RowStride = imageStride i,
                                  PArray.rgb8PixelStride = imageBpp i `div` imageBpc i,
                                  PArray.rgb8IndexRed = 0,
                                  PArray.rgb8IndexGreen = 1,
                                  PArray.rgb8IndexBlue = 2,
                                  PArray.rgb8Data = vec }
  where
    i = imageInfo img

-- |Returns the color space of the given 'Image'
getColorSpace :: Image -> IO CSpace.ColorSpace
getColorSpace img = withImage img (cgImageGetColorSpace >=> CSpace.unsafeCreate)

-- |Creates an 'Image' from the given raw 'CGImageRef'.
-- The pointer must not be used afterwards.
unsafeCreate :: CGImageRef -> IO Image
unsafeCreate imgref = Image <$> toForeignPtr imgref

-- |Passes the underlying CGImageRef to the given function and returns its IO action
withImage :: Image -> (CGImageRef -> IO a) -> IO a
withImage (Image fptr) = withForeignPtr fptr
