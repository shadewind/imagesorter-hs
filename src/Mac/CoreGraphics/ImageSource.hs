{-# LANGUAGE DeriveDataTypeable #-}

{-|
Module     : Mac.CoreGraphics.ImageSource
Maintainer : Emil Eriksson <shadewind@gmail.com>

Haskell interface for CGImageSource in ImageIO
-}
module Mac.CoreGraphics.ImageSource (
  ImageSource,
  ImageSourceException(..),
  Status(..),
  fromPath,
  imageCount,
  status,
  createImage,
  imageFromPath
  ) where

import Foreign.Ptr
import Foreign.ForeignPtr
import Mac.CoreFoundation.Raw
import Mac.CoreFoundation.Utils
import Mac.CoreGraphics.Raw
import qualified Mac.CoreGraphics.Image as Image
import Data.Maybe
import Control.Exception
import Control.Monad
import Data.Typeable

-- |Binding to CGImageSource in ImageIO
newtype ImageSource = ImageSource (ForeignPtr CFType)

-- |Describes the status of an 'ImageSource'
data Status = UnexpectedEOF
            | InvalidData
            | UnknownType
            | ReadingHeader
            | Incomplete
            | Complete
            deriving (Eq, Show)

-- |Thrown by 'ImageSource' functions to indicate errors.
data ImageSourceException = ImageSourceException String deriving (Typeable, Show)
instance Exception ImageSourceException

-- |Creates an 'ImageSource' from a 'FilePath'
fromPath :: FilePath -> IO ImageSource
fromPath path = withCFURL path $ \url -> do
  ptr <- cgImageSourceCreateWithURL url nullPtr
  when (ptr == nullPtr) $
    throwIO (ImageSourceException "Unable to create image source")
  fptr <- toForeignPtr ptr
  return $ ImageSource fptr

-- |Returns the number of images in the given 'ImageSource'
imageCount :: ImageSource -> IO Int
imageCount (ImageSource fptr) = withForeignPtr fptr $ \ptr ->
  fromIntegral `fmap` cgImageSourceGetCount ptr

-- |Returns the 'Status' of an 'ImageSource'
status :: ImageSource -> IO Status
status (ImageSource fptr) = withForeignPtr fptr $ \ptr -> do
  nativeStatus <- cgImageSourceGetStatus ptr
  let mapping = [ (kCGImageStatusUnexpectedEOF, UnexpectedEOF),
                  (kCGImageStatusInvalidData, InvalidData),
                  (kCGImageStatusUnknownType, UnknownType),
                  (kCGImageStatusReadingHeader, ReadingHeader),
                  (kCGImageStatusIncomplete, Incomplete),
                  (kCGImageStatusComplete, Complete) ]
  return $ fromMaybe InvalidData $ lookup nativeStatus mapping

-- |Creates the image at the given index
createImage :: ImageSource -> Int -> IO Image.Image
createImage (ImageSource fptr) index =
  withForeignPtr fptr $ \src -> do
    img <- cgImageSourceCreateImageAtIndex
             src (fromIntegral index) nullPtr
    when (img == nullPtr) $
      throwIO $ ImageSourceException ("Unable to create image at index " ++ show index)
    Image.unsafeCreate img

-- |Convenience function for loading an image from a path. Creates an
-- 'ImageSource' from the given path and creates the image at index @0@
imageFromPath :: FilePath -> IO Image.Image
imageFromPath path = do
  source <- fromPath path
  createImage source 0
