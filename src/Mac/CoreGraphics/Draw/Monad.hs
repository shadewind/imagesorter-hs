{-|
Module     : Mac.CoreGraphics.Draw.Monad
Maintainer : Emil Eriksson <shadewind@gmail.com>

Drawing operations for CoreGraphics
-}

module Mac.CoreGraphics.Draw.Monad (
  Draw(..),
  RGBA(..),
  runDraw,
  createImage,
  setFillColorRGBA,
  fillRect,
  fillEllipse,
  drawImage,
  saveState,
  restoreState,
  bracketDraw,
  getSize,
  getBounds,
  liftDrawingOp,
  liftIO
  ) where

import Control.Applicative
import Mac.CoreGraphics.Draw.Context
import Mac.CoreGraphics.Raw
import Mac.CoreGraphics.Geometry
import Mac.CoreGraphics.Image
import Foreign.Marshal.Utils

-- |Drawing monad which represents a sequence of rendering operations on a
-- 'DrawingContext'
newtype Draw a = Draw (DrawingContext -> IO a)

-- |Executes the given 'render' monad with the given 'DrawingContext'
runDraw :: DrawingContext -> Draw a -> IO a
runDraw ctx (Draw f) = f ctx

instance Functor Draw where
  fmap f a = Draw $ \ctx -> f <$> runDraw ctx a

instance Applicative Draw where
  pure = Draw . const . return
  a <*> b = Draw $ \ctx -> runDraw ctx a <*> runDraw ctx b

instance Monad Draw where
  return = pure
  a >>= b = Draw $ \ctx -> do
    ret <- runDraw ctx a
    runDraw ctx $ b ret

-- |RGBA color value
data RGBA = RGBA Double Double Double Double

-- |Sets the fill color to the given RGB color.
setFillColorRGBA :: RGBA -> Draw ()
setFillColorRGBA (RGBA r g b a) = liftDrawingOp $ \ctx ->
  cgContextSetRGBFillColor ctx
    (toCGFloat r)
    (toCGFloat g)
    (toCGFloat b)
    (toCGFloat a)

-- |Fills a rectangle.
fillRect :: Rect -> Draw ()
fillRect rect = liftDrawingOp $ \ctx -> with rect $ \rectPtr ->
  cgContextFillRect ctx rectPtr

-- |Fills an ellipse in the given rectangle.
fillEllipse :: Rect -> Draw ()
fillEllipse rect = liftDrawingOp $ \ctx -> with rect $ \rectPtr ->
  cgContextFillEllipseInRect ctx rectPtr

-- |Draws the given image in the given rectangle.
drawImage :: Image -> Rect -> Draw ()
drawImage img rect =
  liftDrawingOp $ \ctx ->
  withImage img $ \cgimg ->
  with rect $ \rectPtr ->
    cgContextDrawImage ctx rectPtr cgimg

-- |Saves the current state of the 'DrawingContext'.
saveState :: Draw ()
saveState = liftDrawingOp cgContextSaveGState

-- |Saves the current state of the 'DrawingContext'.
restoreState :: Draw ()
restoreState = liftDrawingOp cgContextRestoreGState

-- |Brackets the given drawing action in 'saveState' and 'restoreState'
bracketDraw :: Draw a -> Draw a
bracketDraw op = do
  saveState
  ret <- op
  restoreState
  return ret

-- |Monadic version of 'Mac.CoreGraphics.Draw.Context.size'
getSize :: Draw (Maybe Size)
getSize = Draw $ return . size
  
-- |Monadic version of 'Mac.CoreGraphics.Draw.Context.bounds'
getBounds :: Draw (Maybe Rect)
getBounds = Draw $ return . bounds

-- |Lifts a rendering operation on a 'CGContextRef' to a 'Render' action.
liftDrawingOp :: (CGContextRef -> IO a) -> Draw a
liftDrawingOp f = Draw $ \ctx -> withContext ctx f

-- |Lifts an IO action into the 'Render' monad.
liftIO :: IO a -> Draw a
liftIO io = Draw $ const io
