{-# LANGUAGE DeriveDataTypeable #-}
{-|
Module     : Mac.CoreGraphics.Draw.Context
Maintainer : Emil Eriksson <shadewind@gmail.com>

'DrawingContext' type and functions for creating it
-}

module Mac.CoreGraphics.Draw.Context (
  DrawingContext,
  DrawingException(..),
  createBitmapContext,
  createBitmapContextRGB8,
  size, bounds,
  createImage,
  unsafeCreate,
  withContext
  ) where

import Mac.CoreFoundation.Raw
import Mac.CoreFoundation.Utils
import Mac.CoreGraphics.Raw
import qualified Mac.CoreGraphics.ColorSpace as CSpace
import qualified Mac.CoreGraphics.Image as Image
import Control.Exception
import Foreign.ForeignPtr
import Foreign.Ptr
import Data.Typeable
import Control.Monad
import Control.Applicative
import Mac.CoreGraphics.Geometry
import System.IO.Unsafe

-- |Wrapper for 'CGContext'
newtype DrawingContext = DrawingContext (ForeignPtr CFType)

-- |Thrown by rendering functions to indicate an error
data DrawingException = DrawingException String deriving (Typeable, Show)
instance Exception DrawingException

-- |Creates a new bitmap context with the given parameters.
createBitmapContext :: Int                -- ^The width
                    -> Int                -- ^The height
                    -> Int                -- ^The number of bits per component
                    -> Int                -- ^The number of bytes per row
                    -> CSpace.ColorSpace  -- ^The 'ColorSpace' to use
                    -> Image.BitmapInfo   -- ^The bitmap info describing the bitmap
                    -> IO DrawingContext  -- ^A new bitmap 'DrawingContext'
createBitmapContext w h bpc bpr cspace bi =
  let bitmapInfo = Image.bitmapInfoToNative bi
      width = fromIntegral w
      height = fromIntegral h
      bitsPerComponent = fromIntegral bpc
      bytesPerRow = fromIntegral bpr
  in CSpace.withColorSpace cspace $ \csref -> do
    cgContextRef <- cgBitmapContextCreate
                      nullPtr
                      width
                      height
                      bitsPerComponent
                      bytesPerRow
                      csref
                      bitmapInfo
    when (cgContextRef == nullPtr) $
      throwIO $ DrawingException "Unable to create bitmap context"
    unsafeCreate cgContextRef

-- |Creates a new 32-bit RGB image
createBitmapContextRGB8 :: Int -> Int -> IO DrawingContext
createBitmapContextRGB8 width height = do
  cspace <- CSpace.createDeviceRGB
  let bitmapInfo = Image.BitmapInfo (Image.Last Image.Skipped) Image.Default
  createBitmapContext width height 8 (4 * width) cspace bitmapInfo

-- |Returns the size of the given bitmap context. Returns 'Nothing' if the context is not a
-- bitmap context.
size :: DrawingContext -> Maybe Size
size ctx = unsafeDupablePerformIO $ withContext ctx $ \ctxref -> do
  width <- cgBitmapContextGetWidth ctxref
  height <- cgBitmapContextGetHeight ctxref
  return $ if (width > 0) && (height > 0)
             then Just $ Size (fromIntegral width) (fromIntegral height)
             else Nothing

-- |Returns the bounding rectangle of the given bitmap context. Return 'Nothing' if the context
-- is not a bitmap context.
bounds :: DrawingContext -> Maybe Rect
bounds ctx = Rect origin <$> size ctx

-- |Creates an image from a bitmap 'DrawingContext'
createImage :: DrawingContext -> IO Image.Image
createImage ctx = withContext ctx (cgBitmapContextCreateImage >=> Image.unsafeCreate)

-- |Creates a 'DrawingContext' from the given raw 'CGContextRef'. The 'CGContextRef' must not be
-- used afterwards.
unsafeCreate :: CGContextRef -> IO DrawingContext
unsafeCreate ctxref = DrawingContext <$> toForeignPtr ctxref

-- |Passes the underlying 'CGContextRef' to the given function and returns the IO action.
withContext :: DrawingContext -> (CGContextRef -> IO a) -> IO a
withContext (DrawingContext fptr) = withForeignPtr fptr
