{-# LANGUAGE FlexibleInstances #-}
{-|
Module     : Mac.CoreGraphics.Draw.Layout
Maintainer : Emil Eriksson <shadewind@gmail.com>

The 'Layoutable' typeclass and assorted instances
-}

module Mac.CoreGraphics.Draw.Layout (
  Layoutable(..),
  Grid,
  listToGrid,
  ImagePath(..)
  ) where

import Mac.CoreGraphics.Geometry
import Mac.CoreGraphics.Draw
import Mac.CoreGraphics.Image
import Data.Array
import Control.Monad
import Mac.CoreGraphics.ImageSource

-- |Typeclass for type that can be drawn within a rectangular bound.
class Layoutable a where
  -- |Draws the given item in the given rectangle.
  drawInRect :: a -> Rect -> Draw ()

instance Layoutable Image where
  drawInRect = drawImage

instance Layoutable RGBA where
  drawInRect color rect = bracketDraw (setFillColorRGBA color >> fillRect rect)

type Grid a = Array (Int, Int) a

instance (Layoutable a) => Layoutable (Grid a) where
  drawInRect arr (Rect rectOrigin size) = forM_ (assocs arr) $ \(i, item) ->
    drawInRect item $ Rect (rectOrigin .+ tileSize .* toSize i) tileSize
    where toSize (w, h) = Size (fromIntegral w) (fromIntegral h)
          (_, maxi) = bounds arr
          tileSize = size ./ (toSize maxi .+ Size 1 1)

-- |Lays out the given list of items in a square grid of the minimum size required
-- to fit all the items. Some items may be repeated if the grid has more cells than the list.
listToGrid :: [a] -> Grid a
listToGrid items = listArray ((0, 0), (size - 1, size - 1)) $ take (size * size) $ cycle items
  where size = ceiling ((sqrt $ fromIntegral $ length items) :: Double)

-- |Wrapper around 'FilePath' which allows lazy loading of images while
-- drawing.
newtype ImagePath = ImagePath FilePath

instance Layoutable ImagePath where
  drawInRect (ImagePath path) rect = do
    image <- liftIO $ imageFromPath path
    drawInRect image rect
  
