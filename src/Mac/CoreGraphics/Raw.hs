{-# LANGUAGE ForeignFunctionInterface #-}
module Mac.CoreGraphics.Raw (
  CGFloat,
  
  CGBitmapInfo,
  kCGImageAlphaNone,
  kCGImageAlphaPremultipliedLast,
  kCGImageAlphaPremultipliedFirst,
  kCGImageAlphaLast,
  kCGImageAlphaFirst,
  kCGImageAlphaNoneSkipLast,
  kCGImageAlphaNoneSkipFirst,
  kCGImageAlphaOnly,
  kCGBitmapFloatComponents,
  kCGBitmapByteOrderDefault,
  kCGBitmapByteOrder16Little,
  kCGBitmapByteOrder32Little,
  kCGBitmapByteOrder16Big,
  kCGBitmapByteOrder32Big,
  kCGBitmapByteOrderMask,
  kCGBitmapAlphaInfoMask,

  CGColorSpaceModel,
  kCGColorSpaceModelUnknown,
  kCGColorSpaceModelMonochrome,
  kCGColorSpaceModelRGB,
  kCGColorSpaceModelCMYK,
  kCGColorSpaceModelLab,
  kCGColorSpaceModelDeviceN,
  kCGColorSpaceModelIndexed,
  kCGColorSpaceModelPattern,
  
  CGImageSourceStatus,
  kCGImageStatusUnexpectedEOF,
  kCGImageStatusInvalidData,
  kCGImageStatusUnknownType,
  kCGImageStatusReadingHeader,
  kCGImageStatusIncomplete,
  kCGImageStatusComplete,

  CGImageRef,
  cgImageGetDataProvider,
  cgImageGetWidth,
  cgImageGetHeight,
  cgImageGetBytesPerRow,
  cgImageGetBitsPerPixel,
  cgImageGetBitsPerComponent,
  cgImageGetBitmapInfo,
  cgImageGetColorSpace,

  CGColorSpaceRef,
  cgColorSpaceGetNumberOfComponents,
  cgColorSpaceGetModel,
  cgColorSpaceCreateDeviceRGB,
  
  CGImageSourceRef,
  cgImageSourceCreateWithURL,
  cgImageSourceGetCount,
  cgImageSourceCreateImageAtIndex,
  cgImageSourceGetStatus,

  CGImageDestinationRef,
  cgImageDestinationCreateWithURL,
  cgImageDestinationAddImage,
  cgImageDestinationFinalize,
  
  CGDataProviderRef,
  cgDataProviderCopyData,

  CGContextRef,
  cgContextSetRGBFillColor,
  cgContextFillRect,
  cgContextFillEllipseInRect,
  cgContextDrawImage,
  cgContextSaveGState,
  cgContextRestoreGState,
  cgBitmapContextCreate,
  cgBitmapContextCreateImage,
  cgBitmapContextGetHeight,
  cgBitmapContextGetWidth
  ) where

import Data.Bits
import Foreign.C
import Foreign.Ptr
import Mac.CoreFoundation.Raw
import Mac.CoreGraphics.Geometry

-- CGBitmapInfo
type CGBitmapInfo = CUInt
kCGBitmapAlphaInfoMask :: CGBitmapInfo
kCGBitmapAlphaInfoMask = 0x1F

kCGImageAlphaNone :: CGBitmapInfo
kCGImageAlphaNone = 0
kCGImageAlphaPremultipliedLast :: CGBitmapInfo
kCGImageAlphaPremultipliedLast = 1
kCGImageAlphaPremultipliedFirst :: CGBitmapInfo
kCGImageAlphaPremultipliedFirst = 2
kCGImageAlphaLast :: CGBitmapInfo
kCGImageAlphaLast = 3
kCGImageAlphaFirst :: CGBitmapInfo
kCGImageAlphaFirst = 4
kCGImageAlphaNoneSkipLast :: CGBitmapInfo
kCGImageAlphaNoneSkipLast = 5
kCGImageAlphaNoneSkipFirst :: CGBitmapInfo
kCGImageAlphaNoneSkipFirst = 6
kCGImageAlphaOnly :: CGBitmapInfo
kCGImageAlphaOnly = 7

kCGBitmapByteOrderMask :: CGBitmapInfo
kCGBitmapByteOrderMask = 0x7000

kCGBitmapFloatComponents :: CGBitmapInfo
kCGBitmapFloatComponents = shiftL 1 8
kCGBitmapByteOrderDefault :: CGBitmapInfo
kCGBitmapByteOrderDefault = shiftL 0 12
kCGBitmapByteOrder16Little :: CGBitmapInfo
kCGBitmapByteOrder16Little = shiftL 1 12
kCGBitmapByteOrder32Little :: CGBitmapInfo
kCGBitmapByteOrder32Little = shiftL 2 12
kCGBitmapByteOrder16Big :: CGBitmapInfo
kCGBitmapByteOrder16Big = shiftL 3 12
kCGBitmapByteOrder32Big :: CGBitmapInfo
kCGBitmapByteOrder32Big = shiftL 4 12

-- CGColorSpaceModel
type CGColorSpaceModel = CInt
kCGColorSpaceModelUnknown :: CGColorSpaceModel
kCGColorSpaceModelUnknown = -1
kCGColorSpaceModelMonochrome :: CGColorSpaceModel
kCGColorSpaceModelMonochrome = 0
kCGColorSpaceModelRGB :: CGColorSpaceModel
kCGColorSpaceModelRGB = 1
kCGColorSpaceModelCMYK :: CGColorSpaceModel
kCGColorSpaceModelCMYK = 2
kCGColorSpaceModelLab :: CGColorSpaceModel
kCGColorSpaceModelLab = 3
kCGColorSpaceModelDeviceN :: CGColorSpaceModel
kCGColorSpaceModelDeviceN = 4
kCGColorSpaceModelIndexed :: CGColorSpaceModel
kCGColorSpaceModelIndexed = 5
kCGColorSpaceModelPattern :: CGColorSpaceModel
kCGColorSpaceModelPattern = 6

type CGImageSourceStatus = CInt
kCGImageStatusUnexpectedEOF :: CGImageSourceStatus
kCGImageStatusUnexpectedEOF = -5
kCGImageStatusInvalidData :: CGImageSourceStatus
kCGImageStatusInvalidData = -4
kCGImageStatusUnknownType :: CGImageSourceStatus
kCGImageStatusUnknownType = -3
kCGImageStatusReadingHeader :: CGImageSourceStatus
kCGImageStatusReadingHeader = -2
kCGImageStatusIncomplete :: CGImageSourceStatus
kCGImageStatusIncomplete = -1
kCGImageStatusComplete :: CGImageSourceStatus
kCGImageStatusComplete = 0

-- CGImage
type CGImageRef = CFTypeRef
foreign import ccall "CGImageGetDataProvider" cgImageGetDataProvider ::
  CGImageRef -> IO CGDataProviderRef
foreign import ccall "CGImageGetWidth" cgImageGetWidth :: CGImageRef -> IO CSize
foreign import ccall "CGImageGetHeight" cgImageGetHeight :: CGImageRef -> IO CSize
foreign import ccall "CGImageGetBytesPerRow" cgImageGetBytesPerRow :: CGImageRef -> IO CSize
foreign import ccall "CGImageGetBitsPerPixel" cgImageGetBitsPerPixel :: CGImageRef -> IO CSize
foreign import ccall "CGImageGetBitsPerComponent" cgImageGetBitsPerComponent :: CGImageRef -> IO CSize
foreign import ccall "CGImageGetBitmapInfo" cgImageGetBitmapInfo :: CGImageRef -> IO CGBitmapInfo
foreign import ccall "CGImageGetColorSpace" cgImageGetColorSpace :: CGImageRef -> IO CGColorSpaceRef

-- CGColorSpace
type CGColorSpaceRef = CFTypeRef
foreign import ccall "CGColorSpaceGetNumberOfComponents" cgColorSpaceGetNumberOfComponents ::
  CGColorSpaceRef -> IO CSize
foreign import ccall "CGColorSpaceGetModel" cgColorSpaceGetModel
  :: CGColorSpaceRef
  -> IO CGColorSpaceModel
foreign import ccall "CGColorSpaceCreateDeviceRGB" cgColorSpaceCreateDeviceRGB
  :: IO CGColorSpaceRef

-- CGImageSource
type CGImageSourceRef = CFTypeRef
foreign import ccall "CGImageSourceCreateWithURL" cgImageSourceCreateWithURL ::
  CFURLRef -> CFDictionaryRef -> IO CGImageSourceRef
foreign import ccall "CGImageSourceGetCount" cgImageSourceGetCount ::
  CGImageSourceRef -> IO CSize
foreign import ccall "CGImageSourceCreateImageAtIndex" cgImageSourceCreateImageAtIndex ::
  CGImageSourceRef -> CSize -> CFDictionaryRef -> IO CGImageRef
foreign import ccall "CGImageSourceGetStatus" cgImageSourceGetStatus ::
  CGImageSourceRef -> IO CGImageSourceStatus

-- CGImageDestination
type CGImageDestinationRef = CFTypeRef
foreign import ccall "CGImageDestinationCreateWithURL" cgImageDestinationCreateWithURL
   :: CFURLRef
   -> CFStringRef
   -> CSize
   -> CFDictionaryRef
   -> IO CGImageDestinationRef

foreign import ccall "CGImageDestinationAddImage" cgImageDestinationAddImage
   :: CGImageDestinationRef
   -> CGImageRef
   -> CFDictionaryRef
   -> IO ()

foreign import ccall "CGImageDestinationFinalize" cgImageDestinationFinalize
   :: CGImageDestinationRef -> IO CFBoolean

-- CGDataProvider
type CGDataProviderRef = CFTypeRef
foreign import ccall "CGDataProviderCopyData" cgDataProviderCopyData
  :: CGDataProviderRef -> IO CFDataRef

-- CGContextRef
type CGContextRef = CFTypeRef

foreign import ccall "CGContextSetRGBFillColor" cgContextSetRGBFillColor
  :: CGContextRef
  -> CGFloat
  -> CGFloat
  -> CGFloat
  -> CGFloat
  -> IO ()

foreign import ccall "CGContextFillRectW" cgContextFillRect
  :: CGContextRef
  -> Ptr Rect
  -> IO ()

foreign import ccall "CGContextFillEllipseInRectW" cgContextFillEllipseInRect
  :: CGContextRef
  -> Ptr Rect
  -> IO ()

foreign import ccall "CGContextDrawImageW" cgContextDrawImage
  :: CGContextRef
  -> Ptr Rect
  -> CGImageRef
  -> IO ()

foreign import ccall "CGContextSaveGState" cgContextSaveGState
  :: CGContextRef -> IO ()
     
foreign import ccall "CGContextRestoreGState" cgContextRestoreGState
  :: CGContextRef -> IO ()

-- CGBitmapContext (CGContextRef)
foreign import ccall "CGBitmapContextCreate" cgBitmapContextCreate
  :: Ptr ()
  -> CSize
  -> CSize
  -> CSize
  -> CSize
  -> CGColorSpaceRef
  -> CGBitmapInfo
  -> IO CGContextRef

foreign import ccall "CGBitmapContextCreateImage" cgBitmapContextCreateImage
  :: CGContextRef -> IO CGImageRef

foreign import ccall "CGBitmapContextGetWidth" cgBitmapContextGetWidth
  :: CGContextRef -> IO CSize

foreign import ccall "CGBitmapContextGetHeight" cgBitmapContextGetHeight
  :: CGContextRef -> IO CSize
