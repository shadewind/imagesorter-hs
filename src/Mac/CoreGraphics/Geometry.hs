{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-|
Module     : Mac.CoreGraphics.Geometry
Maintainer : Emil Eriksson <shadewind@gmail.com>

Geometry types and associated operations
-}

module Mac.CoreGraphics.Geometry (
  CGFloat, toCGFloat, fromCGFloat,
  Point(..),
  origin,
  Size(..),
  zeroSize,
  Rect(..),
  emptyRect,
  Add(..), Sub(..), Mul(..), Div(..)
  ) where

import Control.Applicative
import Foreign.C
import Foreign.Ptr
import Foreign.Storable

-- Common types
type CGFloat = CDouble

-- |Converts a 'Double' to a 'CGFloat'
toCGFloat :: Double -> CGFloat
toCGFloat = CDouble

-- |Converts a 'CGFloat' to 'Double'
fromCGFloat :: CGFloat -> Double
fromCGFloat (CDouble v) = v

-- |2D point with X and Y coordinates
data Point = Point Double Double

-- |The origin, i.e. @Point 0 0@
origin :: Point
origin = Point 0 0

instance Storable Point where
  sizeOf _ = 2 * sizeOf (undefined :: CGFloat)
  alignment _ = alignment (undefined :: CGFloat)
  peek ptr = Point
             <$> (fromCGFloat <$> peekElemOff (castPtr ptr) 0)
             <*> (fromCGFloat <$> peekElemOff (castPtr ptr) 1)
  poke ptr (Point x y) = do pokeElemOff (castPtr ptr) 0 (toCGFloat x)
                            pokeElemOff (castPtr ptr) 1 (toCGFloat y)

-- |2D size with X and Y dimensions
data Size = Size Double Double

-- |Zero size, that is @Size 0 0@
zeroSize :: Size
zeroSize = Size 0 0

instance Storable Size where
  sizeOf _ = 2 * sizeOf (undefined :: CGFloat)
  alignment _ = alignment (undefined :: CGFloat)
  peek ptr = Size
             <$> (fromCGFloat <$> peekElemOff (castPtr ptr) 0)
             <*> (fromCGFloat <$> peekElemOff (castPtr ptr) 1)
  poke ptr (Size x y) = do pokeElemOff (castPtr ptr) 0 (toCGFloat x)
                           pokeElemOff (castPtr ptr) 1 (toCGFloat y)

-- |Rectangle with origin and size
data Rect = Rect Point Size

-- |Empty rectangle, that is @Rect origin zeroSize@
emptyRect :: Rect
emptyRect = Rect origin zeroSize

instance Storable Rect where
  sizeOf _ = sizeOf (undefined :: Point) + sizeOf (undefined :: Size)
  alignment _ = min (alignment (undefined :: Point)) (alignment (undefined :: Size))
  peek ptr = Rect
             <$> peekByteOff ptr 0
             <*> peekByteOff ptr (sizeOf (undefined :: Point))
  poke ptr (Rect o s) = do pokeByteOff ptr 0 o
                           pokeByteOff ptr (sizeOf (undefined :: Point)) s

-- |Class for types which support addition.
class Add a b where
  -- |The result type of the addition.
  type AdditionResult a b
  -- |Returns the sum of the two operands.
  (.+) :: a -> b -> AdditionResult a b
  infixl 6 .+

instance Add Point Size where
  type AdditionResult Point Size = Point
  (Point x y) .+ (Size width height) = Point (x + width) (y + height)

instance Add Size Point where
  type AdditionResult Size Point = Point
  (Size width height) .+ (Point x y) = Point (x + width) (y + height)

instance Add Size Size where
  type AdditionResult Size Size = Size
  (Size w1 h1) .+ (Size w2 h2) = Size (w1 + w2) (h1 + h2)

class Sub a b where
  -- |The result of the subtraction.
  type SubtractionResult a b
  -- |Subtracts the second operand from the first
  (.-) :: a -> b -> SubtractionResult a b
  infixl 6 .-

instance Sub Point Size where
  type SubtractionResult Point Size = Point
  (Point x y) .- (Size width height) = Point (x - width) (y - height)

instance Sub Size Size where
  type SubtractionResult Size Size = Size
  (Size w1 h1) .- (Size w2 h2) = Size (w1 - w2) (h1 - h2)

-- |Class for types which support multiplication.
class Mul a b where
  -- |The result of the multiplication.
  type MultiplicationResult a b
  -- |Returns the result of multiplying the two operands.
  (.*) :: a -> b -> MultiplicationResult a b
  infixl 7 .*

instance Mul Size Size where
  type MultiplicationResult Size Size = Size
  (Size w1 h1) .* (Size w2 h2) = Size (w1 * w2) (h1 * h2)

-- |Class for types that support division.
class Div a b where
  -- |The result of the division.
  type DivisionResult a b
  -- |Returns the result of dividing the first operand by the second operand.
  (./) :: a -> b -> DivisionResult a b
  infixl 7 ./

instance Div Size Size where
  type DivisionResult Size Size = Size
  (Size w1 h1) ./ (Size w2 h2) = Size (w1 / w2) (h1 / h2)
