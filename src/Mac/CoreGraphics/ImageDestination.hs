{-# LANGUAGE DeriveDataTypeable #-}
{-|
Module     : Mac.CoreGraphics.ImageDestination
Maintainer : Emil Eriksson <shadewind@gmail.com>

Wrapper for CGImageDestination
-}

module Mac.CoreGraphics.ImageDestination (
  ImageDestination,
  ImageDestinationException(..),
  create,
  addImage,
  finalize,
  withImageDestination,
  unsafeCreate
  ) where

import Mac.CoreFoundation.Raw
import Mac.CoreFoundation.Utils
import Mac.CoreGraphics.Raw
import qualified Mac.CoreGraphics.Image as Image
import Foreign.ForeignPtr
import Foreign.Ptr
import Control.Exception
import Data.Typeable
import Control.Monad
import Control.Applicative

-- |Wrapper for CGImageDestination
newtype ImageDestination = ImageDestination (ForeignPtr CFType)

-- |Thrown by 'ImageDestination' functions to indicate errors.
data ImageDestinationException = ImageDestinationException String
                               deriving (Typeable, Show)
instance Exception ImageDestinationException

create :: FilePath -> String -> Int -> IO ImageDestination
create path uti size =
  withCFURL path $ \url ->
  withCFString uti $ \utiString -> do
    cfImgDest <- cgImageDestinationCreateWithURL
      url utiString (fromIntegral size) nullPtr
    when (cfImgDest == nullPtr) $
      throwIO (ImageDestinationException "Unable to crate image destination")
    unsafeCreate cfImgDest

-- |Adds an image to the given 'ImageDestination'
addImage :: ImageDestination -> Image.Image -> IO ()
addImage dest img =
  withImageDestination dest $ \cgImgDest ->
  Image.withImage img $ \cgImg ->
    cgImageDestinationAddImage cgImgDest cgImg nullPtr

-- |Finalizes the given 'ImageDestination'
finalize :: ImageDestination -> IO ()
finalize dest = withImageDestination dest $ \cgImgDest -> do
  result <- cgImageDestinationFinalize cgImgDest
  when (result == cfFalse) $
    throwIO (ImageDestinationException "Image destination finalization failed")

-- |Passes the underlying 'CGImageDestinationRef' to the given function and returns the IO action
withImageDestination :: ImageDestination -> (CGImageDestinationRef -> IO a) -> IO a
withImageDestination (ImageDestination dest) = withForeignPtr dest

unsafeCreate :: CGImageDestinationRef -> IO ImageDestination
unsafeCreate cgImgDestRef = ImageDestination <$> toForeignPtr cgImgDestRef
