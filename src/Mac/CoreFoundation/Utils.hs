module Mac.CoreFoundation.Utils (
  withCFType,
  withCFString,
  withCFURL,
  cfStringToString,
  putCFStringLn,
  toForeignPtr
  ) where

import Foreign
import Foreign.C
import Control.Exception
import Mac.CoreFoundation.Raw

withCFType :: IO CFTypeRef -> (CFTypeRef -> IO a) -> IO a
withCFType obj = bracket obj cfRelease

withCFString :: String -> (CFStringRef -> IO a) -> IO a
withCFString str f = withCString str $ \cstr ->
  withCFType (cfStringCreateWithCString nullPtr cstr kCFStringEncodingISOLatin1) f
  
withCFURL :: FilePath -> (CFURLRef -> IO a) -> IO a
withCFURL str f = withCFString str $ \cfstr ->
  withCFType (cfURLCreateWithFileSystemPath nullPtr cfstr kCFURLPOSIXPathStyle cfFalse) f

cfStringToString :: CFStringRef -> IO String
cfStringToString cfstr = do
  len <- cfStringGetLength cfstr
  allocaBytes (fromIntegral $ len + 1) $ \cstr -> do
    _ <- cfStringGetCString cfstr cstr (len + 1) kCFStringEncodingISOLatin1
    peekCString cstr

putCFStringLn :: CFStringRef -> IO ()
putCFStringLn cfstr = cfStringToString cfstr >>= putStrLn

toForeignPtr :: CFTypeRef -> IO (ForeignPtr CFType)
toForeignPtr = newForeignPtr cfReleasePtr

