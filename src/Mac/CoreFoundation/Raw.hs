{-# LANGUAGE ForeignFunctionInterface #-}
module Mac.CoreFoundation.Raw (
  CFType,
  CFTypeRef,
  cfRelease,
  cfReleasePtr,
	
  CFStringRef,
  cfShowStr,
  cfStringGetCString,
  cfStringGetLength,
  cfStringCreateWithCString,
	
  CFURLRef,
  cfURLCreateWithFileSystemPath,
  cfURLGetString,
  
  CFDictionaryRef,
        
  CFDataRef,
  cfDataGetBytePtr,
  cfDataGetLength,

  CFBoolean,
  cfFalse,
  cfTrue,
        
  CFStringEncoding,
  kCFStringEncodingISOLatin1,

  kCFURLPOSIXPathStyle
  ) where

import Foreign.C
import Foreign.Ptr

type CFBoolean = CUChar

type CFAllocatorRef = CFTypeRef
type CFDictionaryRef = CFTypeRef

type CFStringEncoding = CUInt
type CFURLPathStyle = CUInt
type CFIndex = CSize

cfFalse, cfTrue :: CFBoolean
cfFalse = 0
cfTrue = 1

kCFStringEncodingISOLatin1 :: CFStringEncoding
kCFStringEncodingISOLatin1 = 0x0201
kCFURLPOSIXPathStyle :: CUInt
kCFURLPOSIXPathStyle = 0 :: CUInt

-- CFType
data CFType
type CFTypeRef = Ptr CFType
foreign import ccall "CFRelease" cfRelease :: CFTypeRef -> IO ()
foreign import ccall "&CFRelease" cfReleasePtr :: FunPtr (Ptr a -> IO ())

-- CFString
type CFStringRef = CFTypeRef
foreign import ccall "CFStringCreateWithCString" cfStringCreateWithCString ::
  CFAllocatorRef -> CString -> CFStringEncoding -> IO CFStringRef
foreign import ccall "CFShowStr" cfShowStr :: CFStringRef -> IO ()
foreign import ccall "CFStringGetCString" cfStringGetCString ::
  CFStringRef -> CString -> CFIndex -> CFStringEncoding -> IO CFBoolean
foreign import ccall "CFStringGetLength" cfStringGetLength :: CFStringRef -> IO CFIndex

-- CFURL
type CFURLRef = CFTypeRef
foreign import ccall "CFURLCreateWithFileSystemPath" cfURLCreateWithFileSystemPath ::
  CFAllocatorRef -> CFStringRef -> CFURLPathStyle -> CFBoolean -> IO CFURLRef
foreign import ccall "CFURLGetString" cfURLGetString :: CFURLRef -> IO CFStringRef

-- CFData
type CFDataRef = CFTypeRef
foreign import ccall "CFDataGetBytePtr" cfDataGetBytePtr :: CFDataRef -> IO (Ptr CUChar)
foreign import ccall "CFDataGetLength" cfDataGetLength :: CFDataRef -> IO CFIndex
